// Convert port field to KSP PC message

var pc = new ProgramChange;

var lastPort = 0;

function HandleMIDI(event) {

    if(event.port == undefined || event.port < 1) event.port = 1;
    
    if(lastPort != event.port) {
     
        pc.number = event.port;
        pc.channel = event.channel;
        pc.send();

        lastPort = event.port;
    }
   
    event.send();
}

