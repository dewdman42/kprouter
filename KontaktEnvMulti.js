//=============================================================
// Handle NoteOff, like we had to do with CC99 stuff in VePro
// but also convert CC99 to PC messages 1-4
// don't index port value by zero.
//=============================================================


var NeedsTimingInfo = true; //only needed to detect play state

var TRACE = false;
var TRACE99 = false;
var MAXFLUSH = 20;

// array to track the last received port for any given channel
var lastPort = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
var tempLastPort = 0;
var maxPorts = 0;

var pc = new ProgramChange;

//=============================================================
// Actual HandleMIDI function
//=============================================================

function HandleMIDI(event) {
    
    // keep track of the current port for each channel
    // don't forward on the CC99 event
    if(event instanceof ControlChange && event.number == 99) { 
    
        //if( event.value+1 != lastPort[event.channel]) {
        if( event.value+1 != tempLastPort) {
            
            tempLastPort = event.value+1;
            lastPort[event.channel] = tempLastPort;
              
            // convert to PC by number       
            pc.number = event.value+1;
            pc.channel = event.channel;
            pc.port = event.value+1;
            pc.send();
            if(TRACE99) {
                logEvent(event);
            }               
        }
        return;       

    }
    else {
        event.port = lastPort[event.channel];
        if(event instanceof NoteOn && event.port > maxPorts) {
            maxPorts = event.port;
        }
    }
    
    //================================================
    // If all Notes Off for channel, handle specially, 
    // forward to all ports
    //================================================
    
    if (event instanceof ControlChange 
            && event.number == 123 
            && event.value == 0) {

        AllNotesOffByChannel(event.channel);
        return;
    }

    //==============================================
    // INSERT code here for sending keyswitches, etc
    // when inserting keyswitches, send the port attr
    // and call SendByPort(event).  For example:
    //     var keyswitch = new NoteOn;
    //     keyswitch.port = lastPort[event.channel];
    //     SendByPort(keyswitch);
    //==============================================

    // Send actual event 
    event.send();
    logEvent(event);

}


//=========================================
// send VEP multi port encoder in front
// of actual event
//=========================================


function SendByPort(event) {
    
    if(event.port != undefined && event.port > 0) {
        pc.channel = event.channel;
        pc.number = event.port;
        pc.send();
        if(TRACE99) {
            logEvent(cc99);
        }
    }

    event.send();
    logEvent(event);
}


//===================================================
// Prcoess MIDI is used to detact transport STOP
// in order to send AllNotesOff to all ports/channels
//===================================================

var playState = false;

function ProcessMIDI() {
    var ctx = GetTimingInfo();
    
    if (ctx.playing) {
        playState = true;
    } 
    
    else {
        if (playState == true) {
            // stop recently happened
            AllNotesOff();
            playState = false;
        }
    }
}

//====================================
// AllNotesOff all Channels
//====================================

function AllNotesOff() {
    for (var chan = 1; chan <= 16; chan++) {
        AllNotesOffByChannel(chan);
    }
    maxPorts = 0;
}

//=======================================
// Propagate AllNotesOff to all ports
// for a given midi channel
//=======================================

var ccOff = new ControlChange;
ccOff.number = 123;
ccOff.value = 0;
// var note = new NoteOn;

function AllNotesOffByChannel(channel) {
    for (var port = 1; port <= maxPorts; port++) {
        ccOff.channel = channel;
        ccOff.port = port;
        SendByPort(ccOff);
    }
}

//===================================
// Buffered Tracing if turned on
//===================================

var buffer = [];

function logEvent(event) {
    var port = (event.port == undefined || event.port < 1) 
                ? "" : (" [port:"+event.port.toString().padStart(2, "0")+"]");

    logMsg(event.toString() + port);
}

function logMsg(str) {
    if(TRACE) {
        buffer.push(str);
    }
}

function logErr(str) {
        buffer.push(str);
}

function Idle() {
    for (var i = 0; i < MAXFLUSH; i++) {
        if (buffer.length > 0) {
            Trace(buffer.shift());
        }
    }
}

//======================================================
//
// TODO
//
// - Can this be simplified?  Does it need to be as complicated as
//   the Vep macro one?
//
// - STOP is still leaving some notes hanging.  Why?  Need to figure
//   that out in order for this to be usable.  Anyway, VePro is probably
//   easier just use that.  remember core processing is better if spread
//   to more kontakt instances...its really only relevant for articulation
//   stuff
//
// - Should we try to reduce sending of PC to only when it changes per 
//   channel?  KSP has to be updated for that to work
//
// - Since we're using CC99 from old macro, might as well inject byte2
//   here in order to keep full PC range functionality in kontakt
//   maybe its more complicated and confusing though.
//4
// - See if possible to detect transport STOP without 
//   NeedsTimingInfo
//
// - Research why PANIC button LPX sends NoteOff messages to 
//   midi pitches 123-127 also after sending CC123.
//   Do we need to clone that to all ports also?
//
// - See if we can find another way to handle ALL NOTES OFF
//   to all ports besides this script.
//
// - Newer buffered logging
//
// - In order to make it a little more efficient, don't convert
//   events toString until during Idle() when tracing is on.
//
//======================================================

