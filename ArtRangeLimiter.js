/*
 * ArtRangeLimiter
 *
 * V1.0
 *
 * Limits Note evnets to articulationID range in groups of 16.  non-Note 
 * midi events are always passed thru.  Notes without articulationID will be
 * defaulted to articulationID 1.
 ***************************************************************************/

function HandleMIDI(event) {

    // All non notes, just send through
    if(! (event instanceof Note)) {
        event.send();
        return;
    }

    if(event.articulationID == undefined) {
        event.articulationID = 1;      
    }
    
    let outPort = GuiParameter(0);
    let id = event.articulationID;
    
    l
    switch(outPort) {
    case 0:
        event.send();
        break;
    case 1:
        if( id <= 16) event.send();
        break;
    case 2:
        if( id >=17 && id <= 32) event.send();
        break;
    case 3:
        if( id >=33 && id <= 48) event.send();
        break;
    case 4:
        if( id >=49 && id <= 64) event.send();
        break;
    case 5:
        if( id >=65 && id <= 80) event.send();
        break;
    case 6:
        if( id >=81 && id <= 96) event.send();
        break;
    case 7:
        if( id >=97 && id <= 112) event.send();
        break;
    case 8:
        if( id >=113 && id <= 128) event.send();
        break;
    default:
       // TBD, 
       Trace("ERROR, articulationID out of range");
    }
}

var PluginParameters = [];
PluginParameters.push({
    type: "menu",
    name: "Art ID Range",
    valueStrings: ["All","Port 1 (1-16)","Port 2 (17-32)",
                   "Port 3 (33-48)","Port 4 (49-64)", "Port 5 (65-80)",
                   "Port 6 (81-96)","Port 7 (97-112)","port 8 (113-128)"],
    defaultValue: 0,
    disableAutomation: true,
    hidden: false
});

function ParameterChanged(id, val) {
    PluginParameters[id].data = val;
}

// Faster function to get GUI value
function GuiParameter(id) {

    // just in case programmer error
    if(id >= PluginParameters.length) return undefined;
    
    // if script was recently initialized, reload GUI value
    if(PluginParameters[id].data == undefined) {
        PluginParameters[id].data = GetParameter(id);
    }

    return PluginParameters[id].data;
}
