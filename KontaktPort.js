/**********************************************************************
 * KontaktPorts.js
 *
 * Script converts AU3 ports 1-4 into Kontkat ports A-D on midi port 1
 *
 * Info: https://gitlab.com/dewdman42/kprouter/-/blob/master/KontaktPort.js
 *
 *
 * This is handled by sending PC#127 messages with the port encoded in 
 * byte2.
 *
 * A special Kontakt multi-script is required to interpret this on the 
 * kontakt side.  This script can be found at the following 
 * 
 * https://gitlab.com/dewdman42/kprouter/-/blob/master/KontaktPort.ksp
 *
 **********************************************************************/


var pc = new ProgramChange;
pc.number = 127;
var lastPort = 0;

function HandleMIDI(event) {

    if(event.port == undefined || event.port < 1) event.port = 1;
    
    if(lastPort != event.port) {
        lastPort = event.port;
        pc.number = 127;
        pc.data2 = event.port;
        pc.channel = event.channel;
        pc.port = 1;
        event.port = 1;
        
        pc.send();
    }
   
    event.send();
}

